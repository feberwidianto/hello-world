import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import Kemeja from './components/Kemeja';
import Kaos from './components/Kaos';
import Jaket from './components/Jaket';

class App extends Component {

  state = {
    kemeja: [
      {
        gambar:require("./images/umar.webp"),
        nama:"Umar",
        harga: "Rp " + 340000
      },
      {
        gambar:require("./images/abubakar.jpg"),
        nama:"Abu Bakar",
        harga: "Rp " +  350000
      },
      {
        gambar:require("./images/usman.jpg"),
        nama:"Usman",
        harga: "Rp " + 320000
      },
      {
        gambar:require("./images/ali.jpeg"),
        nama:"Ali",
        harga: "Rp " + 330000
      },
      {
        gambar:require("./images/salman.jpg"),
        nama:"Salman Al Farisi",
        harga: "Rp " + 310000
      },
      {
        gambar:require("./images/fatih.jpeg"),
        nama:"Fatih",
        harga: "Rp " + 350000
      },
      {
        gambar:require("./images/husen.JPG"),
        nama:"Husen",
        harga: "Rp " + 250000
      },
      {
        gambar:require("./images/hasan.jpg"),
        nama:"Hasan",
        harga: "Rp " + 250000
      }
    ],
    kaos: [
      {
        gambar:require("./images/zoro.jpg"),
        nama:"Zoro",
        harga: "Rp " + 125000
      },
      {
        gambar:require("./images/luffy.jpg"),
        nama:"Luffy",
        harga: "Rp " + 125000
      },
      {
        gambar:require("./images/brook.jpg"),
        nama:"Brook",
        harga: "Rp " + 125000
      },
      {
        gambar:require("./images/jinbe.jpg"),
        nama:"Jinbe",
        harga: "Rp " + 120000
      },
      {
        gambar:require("./images/chooper.jpg"),
        nama:"Chooper",
        harga: "Rp " + 110000
      },
      {
        gambar:require("./images/sanji.jpg"),
        nama:"Sanji",
        harga: "Rp " + 120000
      },
      {
        gambar:require("./images/usop.jpg"),
        nama:"Usop",
        harga: "Rp " + 100000
      },
      {
        gambar:require("./images/franky.jpg"),
        nama:"Franky",
        harga: "Rp " + 120000
      }
    ],
    jaket: [
      {
        gambar:require("./images/1.jpg"),
        nama:"Asta",
        harga: "Rp " + 230000
      },
      {
        gambar:require("./images/2.jpg"),
        nama:"Kaneki",
        harga: "Rp " + 230000
      },
      {
        gambar:require("./images/3.jpg"),
        nama:"Rurouni",
        harga: "Rp " + 290000
      },
      {
        gambar:require("./images/4.jpg"),
        nama:"Hyuga",
        harga: "Rp " + 200000
      },
      {
        gambar:require("./images/5.jpg"),
        nama:"Abarai",
        harga: "Rp " + 220000
      },
      {
        gambar:require("./images/6.jpg"),
        nama:"Mob",
        harga: "Rp " + 280000
      },
      {
        gambar:require("./images/7.jpeg"),
        nama:"Happy",
        harga: "Rp " + 200000
      },
      {
        gambar:require("./images/8.jpg"),
        nama:"Vegeta",
        harga: "Rp " + 250000
      }
    ]
  }

  render() {
    return (
     <Router>
      <div>
        <h4>MY OLSHOP</h4>
        <Link to="/kemeja">Kemeja</Link>
        <Link to="/kaos">Kaos</Link>
        <Link to="/jaket">Jaket</Link>
        <Route path="/kemeja" exact render={props => <Kemeja {...props} product={this.state.kemeja}/>}/>
        <Route path="/kaos" exact render={props => <Kaos {...props} product={this.state.kaos}/>}/>
        <Route path="/jaket" exact render={props => <Jaket {...props} product={this.state.jaket}/>}/>
      </div>
      </Router>
    );
  }
}

export default App;
