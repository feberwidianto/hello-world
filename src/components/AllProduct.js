import React, { Component } from 'react'
import Kemeja from './Kemeja';
import Kaos from './Kaos';
import Jaket from './Jaket';


export default class AllProduct extends Component {
}
  render() {
    return (
      <div>
        <Kemeja product={this.state.kemeja}/>
        <Kaos product={this.state.kaos}/>
        <Jaket product={this.state.jaket}/>
      </div>
    )
  }
}
