import React, { Component } from 'react'

export default class Jaket extends Component {
  render() {

    const listJaket = this.props.product.map(product => {
      return(
        <div>
          <p>{product.nama}</p>
          <p>{product.harga}</p>
          <img src={product.gambar}/>
        </div>
      )
    })

    return (
      <div>
        {[listJaket]}    
      </div>
    )
  }
}
