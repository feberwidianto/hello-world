import React, { Component } from 'react'

export default class Kaos extends Component {
  render() {

    const listKaos = this.props.product.map(product => {
      return(
        <div>
          <p>{product.nama}</p>
          <p>{product.harga}</p>
          <img src={product.gambar}/>
        </div>
      )
    })
    
    return (
      <div>
        {[listKaos]}
      </div>
    )
  }
}
