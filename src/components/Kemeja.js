import React, { Component } from 'react'

export default class Kemeja extends Component {
  render() {
    
    const listKemeja = this.props.product.map(product => {
      return(
        <div>
          <p>{product.nama}</p>
          <p>{product.harga}</p>
          <img src={product.gambar} />  
        </div>
      )
    })

    return (
      <div>
          {[listKemeja]}
      </div>
    )
  }
}
